<!--
SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
SPDX-FileCopyrightText: 2022 x0y0z0 <x0y0z0tn@gmail.com>

SPDX-License-Identifier: MIT
-->

# Archived

The `kernel` repo has been archived. See below for the new homes of various projects.

# Tezos Smart Rollup Kernels

Various kernels and/or crates were developed here, for running on Tezos' [Smart Rollups](https://tezos.gitlab.io/alpha/smart_rollups.html).

**NB** The Kernel SDK, EVM Kernel, TX Kernel were previously maintained here, but have been moved to the
[octez](https://gitlab.com/tezos/tezos) repo. See respectively:
- Kernel SDK: [src/kernel_sdk](https://gitlab.com/tezos/tezos/-/tree/master/src/kernel_sdk)
- EVM Kernel: [src/kernel_evm](https://gitlab.com/tezos/tezos/-/tree/master/src/kernel_evm)
- Transaction kernel: [src/kernel_tx_demo](https://gitlab.com/tezos/tezos/-/tree/master/src/kernel_tx_demo)

For various other example kernels, see the [Kernel Gallery](https://gitlab.com/tezos/kernel-gallery)

---

## Setup
Either native setup or docker are supported.

### Native
To get started, install **rust** using the [rustup](https://rustup.rs/) tool. Rust version **1.66** is required.

You may also need to install `clang`. See the [Dockerfile] for the full list.

You should then be able to add the `wasm` target by running:
```shell
rustup target add wasm32-unknown-unknown
```

### MacOS - prerequisites

When using `OSX`, you will additionally need to run the following:
```shell
brew install llvm
```

In the output of this command, you should see the path that llvm is installed under:
```
If you need to have llvm first in your PATH, run:
 echo 'export PATH="/opt/homebrew/opt/llvm/bin:$PATH"' >> ~/.zshrc
```

You then need to add this to your path when building kernels. Otherwise, the `clang` picked-up 
by the build will not recognise the `wasm32-unknown-unknown` target.

```
export PATH=/usr/local/opt/llvm/bin:$PATH
```

### Dockerfile
Alternatively, development using docker can be enabled with:
```shell
source scripts/cargo-docker.sh
```

## Building
To build the crate, first install cargo make:
```shell
cargo install cargo-make
```

Then run the following to build the TX Kernel:
```shell
cargo make wasm-tx-kernel
```

> To build with signature verification disabled (possibly for tests) instead use `wasm-tx-kernel-no-sig-verif`.

You should now see `kernel_core.wasm` under `target/wasm32-unknown-unknown/release` or possibly
`target/wasm32-unknown-unknown/release/deps` depending on how you built things.

## Running tests
To run tests for all crates, run:
```shell
cargo make test
```
