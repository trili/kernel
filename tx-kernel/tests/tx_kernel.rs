// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Tests covering the top-level behaviour of the *Transactions Kernel*.

extern crate tezos_smart_rollup_host as host;

use tezos_crypto_rs::{
    bls::bls_generate_keypair,
    hash::{self, ContractKt1Hash, HashTrait, PublicKeyBls},
};
use tezos_crypto_rs::{hash::BlsSignature, PublicKeyWithHash};
use tezos_data_encoding::{enc::BinWriter, nom::NomReader, types::Zarith};
use tezos_smart_rollup_encoding::outbox::{OutboxMessage, OutboxMessageTransaction};
use tezos_smart_rollup_encoding::{
    contract::Contract,
    dac::certificate::V0Certificate,
    entrypoint::Entrypoint,
    inbox::ExternalMessageFrame,
    michelson::ticket::StringTicket,
    michelson::{MichelsonPair, MichelsonString},
    public_key_hash::PublicKeyHash,
    smart_rollup::SmartRollupAddress,
};
use tezos_smart_rollup_host::path::{OwnedPath, RefPath};
use tezos_smart_rollup_host::runtime::Runtime;
use tezos_smart_rollup_mock::MockHost;
use tezos_smart_rollup_mock::TransferMetadata;
use tx_kernel::{
    inbox::{
        external::testing::gen_ed25519_keys,
        sendable::ExternalInboxMessage,
        v1::{sendable::Batch, Operation, OperationContent},
        Signer,
    },
    storage::{account_path, get_or_set_ticket_id, init_account_storage},
    transactions_run,
};

#[test]
fn tx_deposit_then_withdraw_to_same_address() {
    let mut host = MockHost::default();
    let self_address = SmartRollupAddress::new(Runtime::reveal_metadata(&host).address());

    // Arrange
    let (pk, sk) = gen_ed25519_keys();
    let address = pk.pk_hash().unwrap();
    let signer = Signer::PublicKey(pk);

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );
    let contents = "Hello, Ticket!".to_string();

    let string_ticket =
        StringTicket::new(originator.clone(), contents.clone(), 500).unwrap();

    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let source = "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU";

    // Deposit
    let deposit = MichelsonPair(MichelsonString(address.to_b58check()), string_ticket);

    let transfer_metadata = TransferMetadata::new(sender.clone(), source);

    // Withdrawal
    let string_ticket = StringTicket::new(
        Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        ),
        "Hello, Ticket!".to_string(),
        450,
    )
    .unwrap();

    let withdrawal = OperationContent::withdrawal(
        sender.clone(),
        string_ticket,
        Entrypoint::default(),
    );
    let operation = Operation {
        signer,
        counter: 0,
        contents: withdrawal,
    };
    let batch = Batch::new(vec![(operation, sk.clone())]);
    let payload = ExternalInboxMessage::OpList(batch);
    let mut buf = vec![];
    payload
        .bin_write(&mut buf)
        .expect("Failed to serialize payload");
    let external = ExternalMessageFrame::Targetted {
        address: self_address,
        contents: buf,
    };

    // Act
    host.add_transfer(deposit, &transfer_metadata);
    host.add_external(external);

    let starting_level = host.run_level(transactions_run);

    // Assert: withdrawal outbox message at level one
    let output = host.outbox_at(starting_level);

    assert_eq!(1, output.len(), "There should be a single outbox message");

    let outbox_message = output.get(0).unwrap();

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message)
        .expect("Parsing outbox message should work");

    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::<StringTicket>::AtomicTransactionBatch(transactions) => {
            transactions
        }
    };
    assert_eq!(1, transactions.len(), "Expected single outbox transaction");

    let string_ticket = StringTicket::new(
        Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        ),
        "Hello, Ticket!".to_string(),
        450,
    )
    .unwrap();
    let expected = OutboxMessageTransaction {
        parameters: string_ticket.into(),
        destination: Contract::Originated(sender),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(expected, transactions[0]);
}

// Test coverring:
// - ability to withdraw tickets in an account
// - ability to transfer tickets between accounts, as part of a transaction
// - transactions should fail when there is insufficient balance to perform an operation
#[test]
fn tx_deposit_transfer_withdraw() {
    // Arrange
    let rollup_destination = "sr1VHXUw1hMueFzWKYnoXYByFiKxQz4BRpGm";

    // signer representing first account
    let (fst_pk, fst_sk) = gen_ed25519_keys();
    let fst_address = fst_pk.pk_hash().unwrap();
    let fst_signer = Signer::PublicKey(fst_pk);

    // signer representing second account
    let (snd_pk, snd_sk) = gen_ed25519_keys();
    let snd_address = snd_pk.pk_hash().unwrap();
    let snd_signer = Signer::PublicKey(snd_pk);

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );

    // A ticket that will be deposited into account 1, and transferred to account 2
    let make_fst_ticket = |amount: u64| {
        StringTicket::new(originator.clone(), "Hello, ticket".to_string(), amount)
            .unwrap()
    };
    // A ticket that will be deposited into account 2, and transferred to account 1
    let make_snd_ticket = |amount: u64| {
        StringTicket::new(originator.clone(), "Goodbye, ticket".to_string(), amount)
            .unwrap()
    };

    let source =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let sender = "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU";

    let transfer_metadata = TransferMetadata::new(source.clone(), sender);

    // Deposit

    // deposit first ticket into account 1
    let fst_deposit = MichelsonPair(
        MichelsonString(fst_address.to_b58check()),
        make_fst_ticket(300),
    );

    // deposit second ticket into account 2
    let snd_deposit = MichelsonPair(
        MichelsonString(snd_address.to_b58check()),
        make_snd_ticket(300),
    );

    // Withdrawal

    // withdraw 100 of the first ticket
    let fst_withdrawal = || {
        OperationContent::withdrawal(
            source.clone(),
            make_fst_ticket(100),
            Entrypoint::default(),
        )
    };
    // withdraw 98 of the second ticket
    let snd_withdrawal = || {
        OperationContent::withdrawal(
            source.clone(),
            make_snd_ticket(98),
            Entrypoint::default(),
        )
    };

    // Transfer
    let snd_to_fst_account_transfer = || {
        let amount = 99;
        let ticket = make_snd_ticket(amount);
        let hash = ticket.hash().unwrap();
        OperationContent::transfer(fst_address.clone(), hash, amount).unwrap()
    };
    let fst_to_snd_account_transfer = || {
        let amount = 150;
        let ticket = make_fst_ticket(amount);
        let hash = ticket.hash().unwrap();
        OperationContent::transfer(snd_address.clone(), hash, amount).unwrap()
    };

    let valid_batch = vec![
        (
            Operation {
                signer: fst_signer.clone(),
                counter: 0,
                contents: fst_to_snd_account_transfer(),
            },
            fst_sk.clone(),
        ),
        (
            Operation {
                signer: snd_signer.clone(),
                counter: 0,
                contents: snd_to_fst_account_transfer(),
            },
            snd_sk.clone(),
        ),
    ];

    let withdrawal_only = vec![
        (
            Operation {
                signer: Signer::Tz1(fst_address.clone()), // bls key now in cache
                counter: 1,
                contents: snd_withdrawal(),
            },
            fst_sk.clone(),
        ),
        (
            Operation {
                signer: Signer::Tz1(snd_address.clone()), // bls key now in cache
                counter: 1,
                contents: fst_withdrawal(),
            },
            snd_sk.clone(),
        ),
    ];

    // Act
    let rollup_address = SmartRollupAddress::from_b58check(rollup_destination).unwrap();
    let mut mock_host = MockHost::with_address(&rollup_address);

    let valid_batch = Batch::new(valid_batch);
    let valid_message = ExternalInboxMessage::OpList(valid_batch);
    let mut buf = vec![];
    valid_message.bin_write(&mut buf).unwrap();
    let valid_message = ExternalMessageFrame::Targetted {
        address: rollup_address.clone(),
        contents: buf,
    };

    let withdrawal_batch = Batch::new(withdrawal_only);
    let withdrawal_message = ExternalInboxMessage::OpList(withdrawal_batch);
    let mut buf = vec![];
    withdrawal_message.bin_write(&mut buf).unwrap();
    let withdrawal_message = ExternalMessageFrame::Targetted {
        address: rollup_address,
        contents: buf,
    };

    mock_host.add_transfer(fst_deposit, &transfer_metadata);
    mock_host.add_transfer(snd_deposit, &transfer_metadata);
    mock_host.add_external(valid_message);
    mock_host.add_external(withdrawal_message);

    let level = mock_host.run_level(transactions_run);

    // Assert: withdrawal outbox message at level one
    let outputs = mock_host.outbox_at(level);

    assert_eq!(2, outputs.len(), "There should be two withdrawals");

    // First withdrawal:
    // - withdraw ticket 1 from account 1
    let outbox_message = outputs.get(0).unwrap();

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message)
        .expect("Parsing outbox message should work");
    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let fst_transactions = match outbox_message {
        OutboxMessage::<StringTicket>::AtomicTransactionBatch(transactions) => {
            transactions
        }
    };
    assert_eq!(1, fst_transactions.len(), "Expected single outbox message");

    let fst_expected = OutboxMessageTransaction {
        parameters: make_snd_ticket(98).into(),
        destination: Contract::Originated(source.clone()),
        entrypoint: Entrypoint::default(),
    };
    // Second withdrawal
    // - withdraw ticket 2 from account 2
    let outbox_message = outputs.get(1).unwrap();

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message)
        .expect("Parsing outbox message should work");
    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let snd_transactions = match outbox_message {
        OutboxMessage::<StringTicket>::AtomicTransactionBatch(transactions) => {
            transactions
        }
    };
    assert_eq!(1, snd_transactions.len(), "Expected single outbox message");

    let snd_expected = OutboxMessageTransaction {
        parameters: make_fst_ticket(100).into(),
        destination: Contract::Originated(source.clone()),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(fst_expected, fst_transactions[0]);
    assert_eq!(snd_expected, snd_transactions[0]);
}

#[test]
fn tx_filter_internal_transfers_not_addressed_to_rollup() {
    // setup rollup state
    let mut mock_host = MockHost::default();

    // setup message
    let (pk, _sk) = gen_ed25519_keys();
    let receiver = pk.pk_hash().unwrap();

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );
    let contents = "Hello, Ticket!".to_string();
    let string_ticket =
        StringTicket::new(originator.clone(), contents.clone(), 500).unwrap();
    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();
    let source =
        PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();

    // Transfers
    // transfer 500
    let transfer1 = MichelsonPair(
        MichelsonString(receiver.to_b58check()),
        StringTicket::new(originator.clone(), contents.clone(), 500).unwrap(),
    );

    // apply transfer 100
    let transfer2 = MichelsonPair(
        MichelsonString(receiver.to_b58check()),
        string_ticket.testing_clone(),
    );

    let mut transfer_metadata = TransferMetadata::new(sender, source);
    mock_host.add_transfer(transfer1, &transfer_metadata);

    // ignore second (different rollup address)
    transfer_metadata.override_destination(
        SmartRollupAddress::from_b58check("sr1Uz7zvuKLjWdK2SfnA5Axxvxfq94PoZNKz")
            .unwrap(),
    );
    mock_host.add_transfer(transfer2, &transfer_metadata);

    mock_host.run_level(transactions_run);

    let account_storage = init_account_storage().unwrap();
    let receiver_account = account_storage
        .get(&mock_host, &account_path(&receiver).unwrap())
        .unwrap()
        .unwrap();

    let ticket_amount = receiver_account
        .ticket_amount(&mock_host, 0)
        .unwrap()
        .unwrap();

    assert_eq!(500, ticket_amount);
}

#[test]
fn tx_handle_dac_messages() {
    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT18fUtgDECoNHFyM5JmVq7XufVsgy8YcVzo").unwrap(),
    );

    //helper definitions for creating deposits, transfers and withdrawals.
    let source =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let sender = "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU";
    let ticket_content = "Hello, world";

    let transfer_metadata = TransferMetadata::new(source.clone(), sender);

    let make_deposit = |destination_pk: &hash::PublicKeyEd25519, ticket_amount: u64| {
        let address = destination_pk.pk_hash().unwrap();
        let ticket = StringTicket::new(
            originator.clone(),
            ticket_content.to_string(),
            ticket_amount,
        )
        .unwrap();
        MichelsonPair(MichelsonString(address.to_b58check()), ticket)
    };

    let ticket_hash =
        StringTicket::new(originator.clone(), ticket_content.to_string(), 1)
            .unwrap()
            .hash()
            .unwrap();

    let make_transfer =
        |sender_sk: &hash::SecretKeyEd25519,
         sender_pk: &hash::PublicKeyEd25519,
         ticket_amount: u64,
         destination_pk: &hash::PublicKeyEd25519| {
            let destination_address = destination_pk.pk_hash().unwrap();
            let contents = OperationContent::transfer(
                destination_address,
                ticket_hash.clone(),
                ticket_amount,
            )
            .unwrap();
            (
                Operation {
                    signer: Signer::PublicKey(sender_pk.clone()),
                    counter: 0,
                    contents,
                },
                sender_sk.clone(),
            )
        };

    let make_withdrawal = |sender_sk: &hash::SecretKeyEd25519,
                           sender_pk: &hash::PublicKeyEd25519,
                           ticket_amount: u64| {
        let ticket = StringTicket::new(
            originator.clone(),
            ticket_content.to_string(),
            ticket_amount,
        )
        .unwrap();
        let contents =
            OperationContent::withdrawal(source.clone(), ticket, Entrypoint::default());
        (
            Operation {
                signer: Signer::PublicKey(sender_pk.clone()),
                counter: 0,
                contents,
            },
            sender_sk.clone(),
        )
    };

    let mut mock_host = MockHost::default();

    let self_address =
        SmartRollupAddress::new(Runtime::reveal_metadata(&mock_host).address());

    // Set the number of tx processed per kernel run to 50
    let transactions_per_kernel_run_path: RefPath =
        RefPath::assert_from(b"/kernel/transactions.per.kernel.run");

    mock_host
        .store_write(&transactions_per_kernel_run_path, &25_i32.to_le_bytes(), 0)
        .unwrap();

    //generate 100 accounts:
    // 4 of ticket "Hello ticket!" will be deposited to the first 50 accounts,
    // 2 of ticket "Hello ticket!" will be transferred from account[i] to account[i + 50], for i = 0..50
    // 1 of ticket "Hello ticket!" will be withdrawn by account[i+50], for i = 0..50 "
    // generate sender accounts and add deposits to mock rollup host
    let mut transfer_source_accounts = Vec::with_capacity(50);
    for _i in 0..50 {
        let (pk, sk) = gen_ed25519_keys();
        let deposit = make_deposit(&pk, 4);
        transfer_source_accounts.push((pk, sk));
        mock_host.add_transfer(deposit, &transfer_metadata);
    }

    // generate destination accounts, as well as the transfers and withdrawals that will go into a Dac payload

    let mut transfer_destination_accounts = Vec::with_capacity(50);
    let mut operations = Vec::new();
    for (transfer_source_pk, transfer_source_sk) in transfer_source_accounts.iter() {
        let (destination_pk, destination_sk) = gen_ed25519_keys();
        let transfer =
            make_transfer(&transfer_source_sk, &transfer_source_pk, 2, &destination_pk);
        operations.push(transfer);
        let withdrawal = make_withdrawal(&destination_sk, &destination_pk, 1);
        operations.push(withdrawal);
        transfer_destination_accounts.push((destination_pk, destination_sk));
    }
    let valid_batch = Batch::new(operations);

    let mut buf = vec![];
    valid_batch.bin_write(&mut buf).unwrap();

    // compute root hash and make dac preimages available to mockup host
    let root_hash =
        tezos_smart_rollup_encoding::dac::prepare_preimages(&buf, |_hash, preimage| {
            mock_host.set_preimage(preimage);
        })
        .unwrap();

    // Generate Dac committee and store public keys in mock host durable store
    let (dac_member1_sk, dac_member1_pk) = bls_generate_keypair().unwrap();
    let (dac_member2_sk, dac_member2_pk) = bls_generate_keypair().unwrap();

    let mut pk1_buf: Vec<u8> = Vec::with_capacity(PublicKeyBls::hash_size());
    dac_member1_pk
        .bin_write(&mut pk1_buf)
        .expect("Cannot serialize 1st committee member pk");

    let mut pk2_buf: Vec<u8> = Vec::with_capacity(PublicKeyBls::hash_size());
    dac_member2_pk
        .bin_write(&mut pk2_buf)
        .expect("Cannot serialize 1st committee member pk");

    let dac_committee_member_path_prefix: RefPath =
        RefPath::assert_from(b"/kernel/dac.committee");

    let pk1_path = tezos_smart_rollup_host::path::concat(
        &dac_committee_member_path_prefix,
        &OwnedPath::try_from(format!("/0")).unwrap(),
    )
    .unwrap();

    let pk2_path = tezos_smart_rollup_host::path::concat(
        &dac_committee_member_path_prefix,
        &OwnedPath::try_from(format!("/1")).unwrap(),
    )
    .unwrap();

    mock_host.store_write(&pk1_path, &pk1_buf, 0).unwrap();
    mock_host.store_write(&pk2_path, &pk2_buf, 0).unwrap();

    // construct the certificate of data availability
    let mut message1 =
        Vec::with_capacity(root_hash.as_ref().len() + PublicKeyBls::hash_size());

    message1.append(&mut pk1_buf.to_vec());
    message1.append(&mut root_hash.as_ref().to_vec());

    let mut message2 =
        Vec::with_capacity(root_hash.as_ref().len() + PublicKeyBls::hash_size());

    message2.append(&mut pk2_buf.to_vec());
    message2.append(&mut root_hash.as_ref().to_vec());

    let signature1 = dac_member1_sk.sign(root_hash.as_ref()).unwrap();
    let signature2 = dac_member2_sk.sign(root_hash.as_ref()).unwrap();
    let aggregated_signature =
        BlsSignature::aggregate_sigs(&[&signature1, &signature2]).unwrap();
    // least (value: 1) and second least (value: 2) significant bit of the witness field must be set,
    // hence the witness field must be set to 1 + 2 = 3 in the certificate
    let raw_witnesses: [u8; 1] = [3];
    let (_, witnesses) = Zarith::nom_read(&raw_witnesses).unwrap();

    let certificate = V0Certificate {
        root_hash,
        aggregated_signature,
        witnesses,
    };

    let wrapped_message = ExternalInboxMessage::Dac(certificate);

    let mut dac_external_message = Vec::with_capacity(131);
    wrapped_message
        .bin_write(&mut dac_external_message)
        .unwrap();

    let dac_message = ExternalMessageFrame::Targetted {
        address: self_address,
        contents: dac_external_message,
    };
    mock_host.add_external(dac_message);

    let level = mock_host.run_level(transactions_run);

    // ------
    // Assert
    // ------
    assert_eq!(
        transfer_source_accounts.len() + transfer_destination_accounts.len(),
        mock_host
            .store_count_subkeys(&RefPath::assert_from(b"/accounts"))
            .unwrap() as usize,
        "Accounts should be created"
    );

    let accounts = init_account_storage().unwrap();

    let ticket_id = {
        let ticket_id = get_or_set_ticket_id(&mut mock_host, &ticket_hash).unwrap();
        assert_eq!(0, ticket_id);
        ticket_id
    };

    for (pk, _) in transfer_source_accounts.iter() {
        let tz1 = pk.pk_hash().unwrap();
        let path = account_path(&tz1).unwrap();
        let account = accounts.get(&mock_host, &path).unwrap().unwrap();
        assert_eq!(
            Some(2),
            account.ticket_amount(&mock_host, ticket_id).unwrap()
        )
    }

    for (pk, _) in transfer_destination_accounts.iter() {
        let tz1 = pk.pk_hash().unwrap();
        let path = account_path(&tz1).unwrap();
        let account = accounts.get(&mock_host, &path).unwrap().unwrap();
        assert_eq!(
            Some(1),
            account.ticket_amount(&mock_host, ticket_id).unwrap()
        )
    }

    let outbox = mock_host.outbox_at(level);
    assert_eq!(outbox.len(), transfer_destination_accounts.len());
}
