#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
#
# SPDX-License-Identifier: MIT

# Builds & strips the unit test kernel required.

kernel="$1"
root=$(git rev-parse --show-toplevel)

source "$root"/scripts/cargo-docker.sh

cargo build -p test_kernel --target wasm32-unknown-unknown \
    --release --no-default-features --features "$kernel"

cp target/wasm32-unknown-unknown/release/test_kernel.wasm "$root"/"$kernel".wasm

wasm-strip "$root"/"$kernel".wasm
wasm2wat "$root"/"$kernel".wasm > "$root"/"$kernel".wat
